# GitLab Emoji

Gem extending [gemojione gem](https://github.com/jonathanwiesel/gemojione).

# Manual Usage

Copy images to public directory by using the task in `lib/tasks/copy.rake`

# Assets Precompiling

```
# config/application.rb
config.assets.paths << Gemojione.images_path
config.assets.precompile << "emoji/*.png"
```
